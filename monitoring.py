############################################################
#     Monitoring Tool Jan Reimers und Tobias Krabat        #
############################################################
#importing all used packages
import platform, alarm, sys

import argparse
#this will output a help message if the argument -h is given
descriptionhelp="This is a monitoring tool for RAM usage, diskspace and process amount.\n " \
                "1 will monitor the amount of processes \n "\
                "2 will monitor the amount of ram usage\n "\
                "3 will monitor the amount free disk space\n "\
                "4 will monitor the amount of ram and disk usage\n "\
                "5 will monitor the amount of processes and disk usage\n "\
                "6 will monitor the amount of processes and ram usage\n "\
                "7 will monitor all three\n"
parser = argparse.ArgumentParser(description=(descriptionhelp))
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='an integer for deciding what to monitor')

args = parser.parse_args()
#setting the input variable "a" and testing it for being integer
try:
    a = int(sys.argv[1])
except:
    print("For help, restart the program as: python3 monitoring.py -h")
    exit()
#setting the variable os to the current OS
os = platform.system()
#if the current OS is "Linux", set all needed systeminformation as a variable or end the script
if platform.system()!="Linux":
    print("This is a Linux Script")
    exit()

#test if the variable "a" is in the wanted range of 1-7 and then call the wanted functions from alarm.py
#if variable "a" isnt in the wanted range, the script is ended and a help message is displayed
if a in range(1, 8):
    if a == 1:
        alarm.amount_processes()
    if a == 2:
        alarm.ramusage()
    if a == 3:
        alarm.diskusage()
    if a == 4:
        alarm.ramusage()
        alarm.diskusage()
    if a == 5:
        alarm.diskusage()
        alarm.amount_processes()
    if a == 6:
        alarm.ramusage()
        alarm.amount_processes()
    if a == 7:
        alarm.ramusage()
        alarm.diskusage()
        alarm.amount_processes()
else:
    print("For help, restart the program as: python3 monitoring.py -h")
