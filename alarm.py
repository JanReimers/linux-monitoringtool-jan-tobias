#importing all used packages
import subprocess, psutil, socket, yagmail


#write all needed system information into variables
proc = subprocess.check_output("ps -ef|wc -l",shell=True)
amountproc = int(proc) -2
rampercent = psutil.virtual_memory().percent
diskused = psutil.disk_usage("/").percent
hostname = socket.gethostname()
from datetime import datetime
now = datetime.now()


#comparing the set variable for the amount of processes to set values and writing a fitting warning into logdatei if needed
def amount_processes():
    if amountproc <190:
        print("There is a manageable amount of processes with a total of:", amountproc)
    elif amountproc <220:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There is a huge amount of processes with a total of: {amountproc} \n")
    elif amountproc >219:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There are too many processes with a total of: {amountproc}! Please end some Processes!\n")
        #logindata for connecting to the Email account
        yag = yagmail.SMTP(user='tobiasjantest@gmail.com', password='Tobiasjan1!')
        #sending the email
        yag.send(to='tobiasjantest@gmail.com', subject='Testing Yagmail', contents=f'{now}-Warning: {hostname} There are too many processes with a total of: {amountproc}! Please end some Processes!')

#comparing the set variable for the amount of RAM used to set values and writing a fitting warning into logdatei if needed
def ramusage():
    if rampercent <60:
        print(f"There is a manageable amount of RAM used with a total of: {rampercent} %")
    elif rampercent <90:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There is a huge amount of RAM used with a total of: {rampercent} % \n")
    elif rampercent >89:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There is too RAM used with a total of: {rampercent} %! Please make some space!\n")
        #logindata for connecting to the Email account
        yag = yagmail.SMTP(user='tobiasjantest@gmail.com', password='Tobiasjan1!')
        #sending the email
        yag.send(to='tobiasjantest@gmail.com', subject='Testing Yagmail', contents=f'{now}-Warning: {hostname} There is too RAM used with a total of: {rampercent}%! Please make some space!')


#comparing the set variable for the amount of diskusage to set values and writing a fitting warning into logdatei if needed
def diskusage():
    if diskused <50:
        print(f"There is a manageable amount of your storage used with a total of:{diskused}%")
    elif diskused <90:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There is a huge part of your storage used with a total of: {diskused} % \n")
    elif diskused >89:
        outF=open("logdatei", "a")
        outF.write(f"{now}-Warning: {hostname} There is too little space on your disk! {diskused} % is used! Please make some space!\n")
        #logindata for connecting to the Email account
        yag = yagmail.SMTP(user='tobiasjantest@gmail.com', password='Tobiasjan1!')
        #sending the email
        yag.send(to='tobiasjantest@gmail.com', subject='Testing Yagmail', contents=f'{now}-Warning: {hostname} There is too little space on your disk! {diskused}% is used! Please make some space!')

